#ifndef DECAL_CONFIG_H
#define DECAL_CONFIG_H

/** \defgroup DecalConfig Decal Configuration Classes
 *
 * \brief All Decal configuration objects required for pixels/columns and full array configuration storage.
 */
/** \defgroup DecalData Decal Data Classes
 *
 * \brief Data objects for handling Decal data from returned Nexys stream data through to individual strip/pad type hits.
 */

// Keep in mind to add std::vector<OBJECT> to the class summary in linkdef.h

#include <vector>
#include <algorithm>
#include <stdio.h>

#include "../stlib/sct_types.h"

#include "TST.h"

unsigned char reverseNibble(unsigned char nibble);

/**
 * \class PixConfig
 * \ingroup DecalConfig
 * \brief Decal Pixel Configuration Container
 *
 * This class holds the configuration of a single DECAL pixel.
 * Apart from Contructor and Accessor methods it really doesn't do much.
 */
class PixConfig{
public:
	PixConfig(unsigned char setVal=1);
	PixConfig(PixConfig &copyMe);
	PixConfig(unsigned char calibDAC, bool polarity, bool mask);
	~PixConfig();
	bool isMasked();
	void setMasked(bool masked);
	bool isNegative();
	bool isPositive();
	void setPolarity(bool positive);
	unsigned char getDAC();
	void setDAC(unsigned char value);
	unsigned char getConfig();
	void setConfig(unsigned char config, unsigned char mask=0x3F);
private:
 	unsigned char pixConfig; // 6 bits per pixel, MSB to LSB: 4 calib dac, 1 polarity, 1 mask
};

/**
 * \class ColumnConfig
 * \ingroup DecalConfig
 * \brief Decal Column Configuration Container
 *
 * This class holds the configuration of a single DECAL column.
 * Apart from Contructor and Accessor methods it really doesn't do much.
 */
class ColumnConfig{
public:
	ColumnConfig(unsigned char defaultPixel=1);
	ColumnConfig(ColumnConfig &copyMe);
	~ColumnConfig();
	PixConfig *getPixConfig(unsigned int pixNumber=0);
	void enable();
	void disable();
private:
	PixConfig *pix[64];
};

/**
 * \class ChipConfig
 * \ingroup DecalConfig
 * \brief Decal Chip Configuration Container
 *
 * This class holds the configuration of all DECAL pixels, structured in columns.
 * Apart from Contructor and Accessor methods it really doesn't do much.
 */
class ChipConfig{
public:
	ChipConfig(unsigned char defaultPixel=1);
	ChipConfig(ChipConfig &copyMe);
	~ChipConfig();
	ColumnConfig *getColConfig(unsigned int colNumber=0);
	void enable();
	void disable();
	void enableCol(unsigned int index);
	void disableCol(unsigned int index);
	void enableRow(unsigned int index);
	void disableRow(unsigned int index);
	void enablePix(unsigned int indexx, unsigned int indexy);
	void disablePix(unsigned int indexx, unsigned int indexy);
private:
	ColumnConfig *columns[64];
};

/// Helper function for byteswapping the ethernet data
uint16_t swapBytes(uint16_t val);

/**
 * \class RawData
 * \ingroup DecalData
 * \brief RawData Helper Class
 *
 * Helper Class for raw 64 Bit data from NEXYS
 */
class RawData{
public:
/// Constructor expects to be able to access 4x16 bit at adress handed over...
	RawData(uint16_t *data=0);
	RawData(const RawData &copyMe);
	~RawData();
/// returns the n-th 8bit content of RawData - zero should be the DECAL header in case isDecal() returned true, doesn't check index (max 7)
	unsigned char getHit(unsigned int index);
	bool isDecal();
private:
	uint16_t m_rawData[4];
};

/**
 * \class StripHit
 * \ingroup DecalData
 * \brief Data container for strip type data.
 *
 * Extracts individual strip hits from a single data channel byte returned by the Decal asic.
 */
class StripHit{
public:
	StripHit(unsigned char data=0) : m_data(data) {}
	StripHit(const StripHit & copyMe) {m_data = copyMe.m_data;}
	~StripHit() {}
	/// Returns the indexed hit, 0 selects the highest two bits, 3 the lowest two bits, referencing the 1st, 2nd, 3rd and 4th strip stored within a hitword.
	unsigned char getSingleHit(unsigned int index) {return ((m_data >> ((3-index)*2)) & 0x3);}
	/// Shifts the data to allow for phase adaptations post-fact
	void movePhase(unsigned int shift) {
		shift = shift % 8; unsigned int mask = (1<<shift) -1;
		m_data = (((m_data >> shift) & mask) | (m_data << (8-shift))) & 0xff;
	}
	unsigned char getData() const {return m_data;}
	unsigned char getSumHits();
	bool operator==(const StripHit& rhs){return (m_data == rhs.getData());}
	bool operator!=(const StripHit& rhs){return (m_data != rhs.getData());}
private:
	unsigned char m_data;
};

/**
 * \class PadHit
 * \ingroup DecalData
 * \brief Decal Pad Hit data container
 *
 * This class allows to cast the content of two raw hit words into a single pad output.
 */
class PadHit{
public:
	PadHit(unsigned char sum=0, unsigned char overflow=0) : m_sum(sum), m_overflow(overflow) {}
	PadHit(const PadHit &copyMe) {m_sum = copyMe.m_sum; m_overflow = copyMe.m_overflow;}
	~PadHit() {}
	unsigned char getSum() const {return m_sum;}
	unsigned char getOverflow() const {return m_overflow;}
	/// Shifts the data to allow for phase adaptations post-fact
	void movePhase(unsigned int shift) {
		shift = shift % 8; unsigned int mask = (1<<shift) -1;
		m_sum = (((m_sum >> shift) & mask) | (m_sum << (8-shift))) & 0xff;
		m_overflow = (((m_overflow >> shift) & mask) | (m_overflow << (8-shift))) & 0xff;
	}
	bool operator==(const PadHit& rhs){return ((m_sum == rhs.getSum()) && (m_overflow == rhs.getOverflow()));}
	bool operator!=(const PadHit& rhs){return ((m_sum != rhs.getSum()) || (m_overflow != rhs.getOverflow()));}
private:
	unsigned char m_sum, m_overflow;
};

/**
 * \class RawDataDecoder
 * \ingroup DecalData
 * \brief RawData Identification Class
 *
 * Helper Class for raw 64 Bit data from NEXYS. Allows to verify whether
 * a 64 bit set of data looks like DECAL data.
 */
class RawDataDecoder{
public:
	RawDataDecoder();
	RawDataDecoder(RawDataDecoder &copyMe);
	~RawDataDecoder();
	/// Adds a new 16 bit word to the end of the set - drops the first 16 bit word
	void addData(uint16_t newVal);
	// Returns true if the current 64 bit set starts with 0x90, identifying the header of DECAL type data.
	bool isValidDecal();
private:
	uint16_t currentData[4];
};

/**
 * \class StreamData
 * \ingroup DecalData
 * \brief StreamData Helper Class
 *
 * Helper Class to handle returned data from NEXYS. Filling is manual, but then
 * allows to retrieve either RawData class from a set of 64 bits, or individual
 * parts of the data.
 * Also has functionality to dump the data to a terminal.
 */
class StreamData{
public:
	/// Empty Constructor
	StreamData();
	/// Copy Constructor
	StreamData(const StreamData &copyMe);
	/// Constructor from data returned from hardware
	StreamData(uint16_t length, uint16_t *data, unsigned int requested);
	~StreamData();
	/// Returns the fill words which are generally not used in this data
	uint16_t getWord(unsigned int index);
	/// Returns real Data Words received from the hardware
	uint16_t getDataWord(unsigned int index);
	/// Returns the Stream number of the stored data
	uint16_t getChannel();
	uint16_t getWords(unsigned int index);
	/// Generates a RawData object, generated from the index*4 -th data word
	RawData getRawData(unsigned int index);
	unsigned int getLength();
	unsigned int getRawLength();
	/// Operator for std::sort, compares Streams by channel number
	bool operator< (const StreamData& str) const;
	StreamData& operator=(const StreamData& copyMe);
	void dump(bool verbose=false);
private:
	uint16_t m_channel; ///< The channel number
	std::vector<uint16_t> m_words; ///< Unused fill words
	std::vector<uint16_t> m_data; ///< Data words
};

#endif /// ends ifndef DECAL_CONFIG_H
