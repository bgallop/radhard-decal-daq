
# Changelog

In reverse time order for convenience

## 15.10.2018

Fixing things:
* Threshold scan shows wrong order of columns (configuration appears from the right of the physical chip, columns should start on the right in the histograms), changing channel order in plotting to fix this.
* Adding a reset function to allow resetting just before capturing data
  * Has an option for delay that runs all the way up to thresholdscan - Delay is in 12.5ns cycles, should vary that to understand the impact on the scan result and therefore the timing of recharge. Range up to some 5 us maybe.
* Fixed up config objects to reflect correct bit ordering - NEEDS A RECOMPILE!!!

## 08.08.2018

Noticed a change in behaviour when slowing down the configuration process, therefore generated slower configuration speeds (through parameter nRepeat in all configuration functions)
UNTESTED

## 02.08.2018

Noticed reverse behaviour of enabling and disabling pixels in configuration, hence reversed the behaviour at the Pixel object level.
Also generated code that loops over configurations and allows holding a specific configuration in memory.

## 27.06.2018

Added a function to retrieve the maximum current of the current sinks based on the used load resistance. **Important: Load resistance should be defined before using the code (see top of DecalMotherboard.h)!**

## 12.06.2018

Updated some code around the threshold scan, in particular fixed the direction of enable/disable in configurations.

## 03.05.2018

Removed duplicates of `operatePixels()` and `calibratePixels()`, and modified `thresholdScan(...)` to use new functions. Minor other fixes here and there.

## 02.05.2018

`operatePixels()` has been filled with correct behaviour. Also added `calibratePixels`, which allows to explicitely connect the DAC.

## 26.03.2018

`runPadCheck()` has been added to test_DECAL.cxx. In the process, added a function to write a whole vector of test values, `inject_numbers(std::vector<unsigned int>, bool)`, in one go so as to optimise speed (std::vectors seem slow in cling).

## 22.03.2018

Prevented `inject_number()` from injecting anything that is not a 5 bit number, if given a larger argument, will cut the bit content down to 5 bits.

## 02.03.2018

`runStripCheck()` has been added to test_DECAL.cxx. Also added a sorting parameter to captureData and re-inserted std::sort functionality. Should make results easier to read.

## 28.02.2018

Added a function to set the Bias voltag, use with caution: `setBias(float)`, `setBias(unsigned int)` will lead to an error message, as the function requires you to know what you're up to.

Added a method to the Strip-/PadData Objects, allowing to re-adjust the data phase.

## 23.02.2018

Adding functionality to allow simple extraction of single strip hits as well as preparing a simple padHit class.
`StripHit` gets initialised with a single `unsigned char` that can be retrieved from `RawData.getHit(...)`. After initialising it, the class allows to retrieve individual hitcounts for the four strips contained within a byte, but using the `getSingleHit(index)` method, where index can be a number from 0 to 3 (returns 0 otherwise).

## 22.02.2018

Had to mess with the Repo today to make sure that the history is correctly recorded. So far all code submitted is mine, so I fear that submits from other peoples machines were re-tagged as mine.

Found a small feature today, added a reset to the transmission periphery and voila, test register reading works. Now working towards an automated check of the return data, so as to do a full test sequence of the digital logic (in strip mode first, then work towards pad mode)

## 21.02.2018

Added new functionality to allow ease of reading:
- Printing numbers as binary (`printBinary`)
- Using a central variable to hold the configuration register value, so as to allow to modify the phase of the manual phase setting without diturbing the remaining configuration (function is `setConfigRegister`)
- Setting the manual phase value `setManualPhase(bool onOff, unsigned int value)`

## 11.02.2018

Bug Introduced on the 11th of January is finally tagged - how long did it take to spot this. Opcode for sending was for RawSigs rather than RawSeq... Fixed, also introduced a verbose flag for sendData.

## 02.02.2018

MANY bugfixes to operate with ROOT 6 on Linux, still going...

## 01.02.2018

Yesterday was a few bugfixes in the code, today is some fixes to compile with ROOT 6 (Latest commit to get rid of a warning).

## 11.01.2018

### Afternoon:

Gathered a few more functions in DecalMotherboard. Attempts to move to a simpler HSIO implementation failed so far - needs a feature branch soon.

Notably `setupDigital()` and `setupAnalog()` are now available. Clock switchOff still needs reading of Status register 33 (function for that is implemented but untested). More to come here.

### Morning:

Removed unused functions, reduced code in existing functions, some moves - Really only a cleanup commit

## 09.01.2018

Moved all files into subdirectories so that their copying structure is clear - also added more doxygen type comments, far from complete, but hey - "Allem Anfang liegt ein Zauber inne"

All files from subfolders (`stdll` and `macros`) now need copying into the corresponding subfolders in your ITSDAQ installation.

## 08.01.2018

Adding comments into the code, hope this clarifies objects in DecalConfig as well as some functions in DecalMotherboard.

## 04.01.2018

Software now stably running with compiled version, need to modify folder structure to make a merge easier.

## 20.12.2017

**Code now works!**

Compilation was actually in order, turns out one has to add `std::vector<StreamData>` to linkdef.h pragmas in stdll, otherwise CINT does not understand these objects from macros.

## 19.12.2017

**Code currently doesn't work with compiled libraries, debugMe() crashes on deletion of StreamData Objects, and I have no clue as to why that happens. Same code in macro version works - will separate out a test_DECAL_macros version in a bit to support macro and compiled operation at the same time.**

## 19.12.2017

Separated out functionality into DecalMotherboard.xxx and DecalConfig.xxx
DecalConfig **should be copied to stdll and** (modifying wscript in ITSDAQ) **compiled into stdll** to be provided as binary library (faster, cleaner, not quite necessary yet)
DecalMotherboard.xxx as well as test_DECAL.cxx now have to go to macros and can then be loaded:

```
.L macros/DecalMotherboard.cpp
.L macros/MattTests.cpp
.L macros/test_DECAL.cxx
```

Functionality as before

If wanting to run without that, please check former versions of test_DECAL, as header references have changed.


## 13.12.2017

Check out into a separate directory, currently not embedded as a submodule within ITSDAQ, then copy over into macro folder and run ITSDAQ  
Within CINT do:

```
.L macros/ConfigUtils.cpp
.L macros/I2C_DECAL.cpp
.L macros/MattTests.cpp
.L macros/test_DECAL.cxx
```

Now you're environment is ready to operate with DECAL

