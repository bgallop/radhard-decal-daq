// Follwing include lines have to commented in to operate with the compiled version of DECAL related Objects
#include "../stdll/DecalConfig.h"
#include "DecalMotherboard.h"
#include "TH2F.h"

void configureColumnOFF() {
	printf("Masking all Pixels\n");
	ColumnConfig *cols = new ColumnConfig(1);
	for (int i=0;i<64;i++) {
		std::cout<< "Generated Pixel " << i << " with configuration " << int(cols->getPixConfig(i)->getConfig()) << std::endl;
	}
	writeColumn(cols);
//	debugConfig(data);
	printf("That was a full Column Config\n");
}

void configureChipOFF() {
	printf("Masking all Pixels\n");
	ColumnConfig *cols = new ColumnConfig(1);
//	debugConfig(data);
	for (int i=0; i<63; i++) {
		writeColumn(cols);
	}
	printf("That was a full Chip Config\n");
}

// Write a threshold scan:
// Configure one Pixel on
// set Threshold over full range
// at each threshold setting run the following
// captureData with parameters for a large set of data, e.g. 64 data words to be returned.
// sum up numbers per channel, then histogram

void thresholdScan_PadMode(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int config) {
	ChipConfig *cfg = new ChipConfig(config);
	writeChip(cfg);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 4, -0.5, 3.5, nSteps, offset-step, offset+range+step);
	TH2D *myOverflowHisto = new TH2D("OverflowThresholdScan", "OverflowThresholdScan", 4, -0.5, 3.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		unsigned int nOverflow=0;
		if (resetCalib) {
			operatePixels(true);
			calibratePixels(true);
			calibratePixels(false);
			operatePixels(false);
		}
		captureData(toBeHistogrammed, nStrobes, 16, true);
    //pad mode, data comes in on channels 3,7,11,15, overflow on 2,6,10,14
    for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {//loop over channels
      if(k%4!=3){continue;} //ignore these channels, don't contain data in pad mode 
      
      for(unsigned int m=0; m< nStrobes*7; m++) {
      	int hits=toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1);
      	int overflow=toBeHistogrammed[k-1].getRawData(m/7).getHit(m%7+1);

      	myHisto->Fill((float)(3-(k-3)/4), (float) ((range/(nSteps-1)*i)+offset),hits);
      	myOverflowHisto->Fill((float)(3-(k-3)/4), (float) ((range/(nSteps-1)*i)+offset),hits);

      	nHits+=hits;
      	nOverflow+=overflow;  	
      }
  }
  std::cout << " Hit Count was: " << nHits <<" Oveflow was: "<<nOverflow<< std::endl;
  toBeHistogrammed.clear();
}
myOverflowHisto->Draw("COLZ");
myOverflowHisto->SaveAs("OverflowThresholdScan.root");
myHisto->Draw("COLZ");
myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScan(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int config, unsigned int delay) {
	ChipConfig *cfg = new ChipConfig(config);
	writeChip(cfg);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, delay);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m< nStrobes*7; m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
	delete cfg;
	cfg = 0;
}

void FirstColCfgTest(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes) {

	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	ChipConfig *cfg = new ChipConfig(0x00);

	for(unsigned int config=0; config<64; config++)
	{  
      //reset the chip cfg to 0x00
		writeChip(cfg);

      //write first col
		std::cout<<"Config is "<<config<<std::endl;
		ColumnConfig *colcfg = new ColumnConfig(config);
		writeColumn(colcfg);

		std::vector<StreamData> toBeHistogrammed;
		operatePixels(true);
		calibratePixels(true);
		calibratePixels(false);
		operatePixels(false);
		for(unsigned int i=0; i<nSteps; i++) {
			setThreshold((float) ((range/(nSteps-1)*i)+offset));
	//std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
			unsigned int nHits=0;
			if (resetCalib) {
				operatePixels(true);
				calibratePixels(true);
				calibratePixels(false);
				operatePixels(false);
			}
			captureData(toBeHistogrammed, nStrobes, 16, true);
	// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
			for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
				for(unsigned int m=0; m< nStrobes*7; m++) {
					StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
					for (unsigned int p=0;p<4;p++) {
						if (hit->getSingleHit(p) && (float)(p+(15-k)*4)>62.5) {
							myHisto->Fill(config, (float) ((range/(nSteps-1)*i)+offset));
							nHits++;
						}
					}
					delete hit;
				}
			}
	//std::cout << " Hit Count was: " << nHits << std::endl;
			toBeHistogrammed.clear();
		}
		delete colcfg;
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}
void thresholdScanConfig(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
// Some examples of what could be done with a configuration
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		if (resetCalib) {
			operatePixels(true);
			calibratePixels(true);
			calibratePixels(false);
			operatePixels(false);
		}
		captureData(toBeHistogrammed, nStrobes, 16, true);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m< nStrobes*7; m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScanByRow(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	for(int rowNum=0; rowNum<64; rowNum++) {
		config->disable();
		config->enableRow(rowNum);
		writeChip(config);
		double step=(range/((double) nSteps-1))/2.0;
		char histoname[200] = "";
		sprintf(histoname,"ThresholdScanRow%02d",rowNum);
		char filename[200] = "";
		sprintf(filename,"ThresholdScanRow%02d.root",rowNum);
		TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
		std::vector<StreamData> toBeHistogrammed;
		operatePixels(true);
		calibratePixels(true);
		calibratePixels(false);
		operatePixels(false);
		for(unsigned int i=0; i<nSteps; i++) {
			setThreshold((float) ((range/(nSteps-1)*i)+offset));
			std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
			unsigned int nHits=0;
			if (resetCalib) {
				operatePixels(true);
				calibratePixels(true);
				calibratePixels(false);
				operatePixels(false);
			}
			captureData(toBeHistogrammed, nStrobes, 16, true);
	// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
			for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
				for(unsigned int m=0; m< nStrobes*7; m++) {
					StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
					for (unsigned int p=0;p<4;p++) {
						if (hit->getSingleHit(p)) {
							myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
							nHits++;
						}
					}
				}
			}
			std::cout << " Hit Count was: " << nHits << std::endl;
			toBeHistogrammed.clear();
		}
		myHisto->Draw("COLZ");
		myHisto->SaveAs(filename);
	}
}

void runStripCheck() {
	std::vector<StreamData> v;
	unsigned int words=1;
	unsigned int nChannels=18;
	unsigned char i=0;
	while (true) {
		StripHit fill = StripHit(i);
		std::vector<unsigned int> values;
		for (unsigned int k=0; k<64; k++) {
			values.push_back(fill.getSingleHit((3-(k%4))));
		}
		inject_numbers(values);
		captureData(v, words, nChannels);
		for (unsigned int k=0; k<16; k++) {
			if (StripHit(v[k].getRawData(0).getHit(1)) != fill) {
				printf("%03d: Error in returned data, Channel %x, Data is %02x, Expected %02x\n", i, v[k].getChannel(), v[k].getRawData(0).getHit(1), fill.getData());
			}
		}
		v.clear();
		if(i==255) break;
		i++;
	}
}

void runPadCheck() {
	std::vector<StreamData> v;
	unsigned int words=1;
	unsigned int nChannels=18;
	unsigned int i=0;
	std::vector<unsigned int> sum2be;
	sum2be.clear();
//	printf("Starting PadCheck...\n");
	while (true) {
//		printf("In Loop, counter %d\n", i);
		unsigned int temp = i;
		sum2be.clear();
		for (unsigned int k=0; k<16; k++) {
			if (temp < 31) {
				sum2be.push_back(temp);
			} else {
				sum2be.push_back(31);
				temp -= 31;
			}
		}
		for (unsigned int blocks = 0; blocks < 4; blocks++) {
			inject_numbers(sum2be);
//			printf("Block Number %d is being configured\n", blocks);
		}
		unsigned int sum = 0;
		unsigned int overflow = 0;
		for (unsigned int index=0; index<sum2be.size(); index++) {
			sum += (sum2be.at(index) & 0xf);
			overflow += ((sum2be.at(index) & 0x10) >> 4);
		}
		PadHit fill = PadHit(sum, overflow);
		captureData(v, words, nChannels);
		for (unsigned int k=2; k<16; k+=4) {
			if (PadHit(v[k+1].getRawData(0).getHit(1), v[k].getRawData(0).getHit(1)) != fill) {
				printf("%03d: Error in returned data, Channel %x, Sum is %02x, Channel %x, Overflow is %02x, Expected %02x and %02x\n", i, v[k+1].getChannel(), v[k+1].getRawData(0).getHit(1), v[k].getChannel(), v[k].getRawData(0).getHit(1), fill.getSum(), fill.getOverflow());
			}

		}
		v.clear();
		i++;
		if(i>31*16) break;
	}
}

void resetChip(bool enable) {
	ChipConfig *cfg = new ChipConfig(0);
// Some examples of what could be done with a configuration
//	cfg->disable(); // disable all pixels
//	cfg->enableRow(row); // enable one row (as per parameters)
	if (enable) {
		cfg->enable();
	} else {
		cfg->disable();
	}
	writeChip(cfg);
}
